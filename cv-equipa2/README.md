## **Atividade Git - CV da Equipa**

#### **Instructions:**
* Criar uma conta no Bitbucket com o email do IEFP
* Organizar com a equipa um líder (ou Scrum Master) para criação do projeto base e organização das branchs de desenvolvimento no Bitbucket

#### **Proposta:**
* Fazer uma breve descrição de cada elemento do grupo (nome, email, educação, interesses e hobbies, linguagens/tecnologias)
* Deve ser feito em um único arquivo (txt, markdown, html, json)

#### **Avaliação:**
* Seguir as recomendações de integração do GitFlow
* Definir os revisores e integrador do Pull Request
* Resolver os conflitos através do merge do git
