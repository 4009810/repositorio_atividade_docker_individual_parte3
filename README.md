## **Atividade Docker - Individual**

#### **Instructions**
##### **`As seguintes atividades são individuais`**
* Criar um container com uma imagem alpine (https://hub.docker.com/_/alpine) e outro container com a mesma imagem. Fazer uma network entre eles. Realizar um ping de um container para o outro.
* Criar um container com uma imagem alpine (https://hub.docker.com/_/alpine) que compartilhe um volume entre o host e o container. Criar um arquivo no host e abrir no container (através do comando "cat")
* **Criar um Dockerfile utilizando o projeto do CV da sua equipa em que seja possível ler este arquivo ao entrar no container (através do comando "cat").**
* **Criar um repositório no Bitbucket e enviar o projeto com o Dockerfile e os arquivos necessários para executar a tarefa**

#### **Avaliação:**
* Para as duas primeiras atividades deve ser realizado um print screen mostrando o resultado final.
* **Para a atividade do Dockerfile, deve ser possível criar um container e visualizar o CV da equipa**

### **Sugestão de Utilização - [Docker](https://labs.play-with-docker.com/)**
```sh
$ git clone https://4009810@bitbucket.org/4009810/repositorio_atividade_docker_individual_parte3.git
$ cd repositorio_atividade_docker_individual_parte3/
$ docker build -t myimage:v1 .
$ docker run --name mycontainer myimage:v1
$ docker logs mycontainer
```
