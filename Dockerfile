FROM alpine

LABEL maintainer="Bruno Felipe <4009810@formacao.iefp.pt>"

COPY cv-equipa2/ /opt/workspace/atividade-docker-individual-parte3

WORKDIR /opt/workspace/atividade-docker-individual-parte3

RUN pwd

RUN ls -a

CMD ["cat","cv-equipa2.txt"]
